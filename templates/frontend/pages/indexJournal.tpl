{**
 * templates/frontend/pages/indexJournal.tpl
 *
 * UPDATED/CHANGED/MODIFIED: Marc Behiels - marc@elemental.ca - 250416
 *
 * Copyright (c) 2014-2016 Simon Fraser University Library
 * Copyright (c) 2003-2016 John Willinsky
 * Distributed under the GNU GPL v2. For full terms see the file docs/COPYING.
 *
 * @brief Display the index page for a journal
 *
 * @uses $currentJournal Journal This journal
 * @uses $journalDescription string Journal description from HTML text editor
 * @uses $homepageImage object Image to be displayed on the homepage
 * @uses $additionalHomeContent string Arbitrary input from HTML text editor
 * @uses $announcements array List of announcements
 * @uses $numAnnouncementsHomepage int Number of announcements to display on the
 *       homepage
 * @uses $issue Issue Current issue
 *}





 {assign var=ThumbPath value = $currentJournal->_data['journalThumbnail']['en_US']['uploadName']}
 


{include file="frontend/components/header.tpl"  pageTitleTranslated=$currentJournal->getLocalizedName()}


<div id="main-content" class="page_index_journal" role="content">
{* /Page Title *}

	{* Page Title *}
	<header class="page-header" style="margin-top:0px;">
		
	</header>
	{* /Page Title *}
	<div class="homepageImage">
           
    	</div>
    <div class="journalInfoCol1">
		





<div class="currentIssueInfo">
	<div class="content">
		<p>
        
        
        {if $issueCover}
                <div class="media-left issue_cover_block{if !$issue->hasDescription()} align-left{/if}">
                    <a href="{url op="view" page="issue" path=$issue->getBestIssueId()}">
                        <img class="cover_image"
                             src="{$issueCover|escape}"{if $issue->getLocalizedCoverImageAltText() != ''} alt="{$issue->getLocalizedCoverImageAltText()|escape}"{/if}>
                    </a>
                </div>
		{/if}
        
        		<p><div class="media-left">
			<a class="cover" href="{url op="view" path=$issue->getBestIssueId($currentJournal)}">
					
				<img class="media-object" src="{$publicFilesDir}/{$issue->getCoverImage('en_US')}"> 
			
			</a>
		</div></p>
        
        
        </p>
	</div>
</div>


{include file="frontend/pages/userFiles/issn.tpl"}




		
				<div class="infojrnl" style="background-image:url(/plugins/themes/responsive/img/homepageImage_en_US.jpg) ; background-position:center center; background-size:cover">
					
				<div class="highlightBar">
					<h2>About the Journal</h2>


					{include file="frontend/pages/userFiles/journalDescription.tpl"}
					
									</div>
			</div>
		
		
	</div>


<div class="journalContentWr">
    <header class="page-header">
                <h3>QUICK LINKS</h3>
            </header>
	{include file="frontend/pages/userFiles/quicklinks.tpl"}
              
	{* Latest issue *}
	{if $issue}
		<section class="current_issue">
			<header class="page-header">
				<h3>
					{translate key="journal.currentIssue"} 
				</h3>
			</header>
			
			{include file="frontend/objects/issue_toc.tpl"}
			<!-- <a href="{url router=$smarty.const.ROUTE_PAGE page="issue" op="archive"}" class="btn btn-primary read-more">
				{translate key="journal.viewAllIssues"}
				<i class="fa fa-angle-right"></i>
			</a> -->
		</section>
	{/if}
	
	{* Announcements *}
	{if $numAnnouncementsHomepage && $announcements|count}
		<section class="cmp_announcements media">
			<header class="page-header">
				<h3>
					{translate key="announcement.announcements"}
				</h3>
			</header>
			<div class="media-list">
				{foreach name=announcements from=$announcements item=announcement}
					{if $smarty.foreach.announcements.iteration > $numAnnouncementsHomepage}
						{break}
					{/if}
					{include file="frontend/objects/announcement_summary.tpl" heading="h3"}
				{/foreach}
			</div>
		</section>
	{/if}

	{* Additional Homepage Content *}
	{if $additionalHomeContent}
		<header class="page-header>
				<h3> Additional Content </h3>
			</header>
		<section class="additional_content">
			{$additionalHomeContent}
		</section>
	{/if}
	</div>

	<!-- static block -->



	{call_hook name="Templates::Index::journal"}

</div><!-- .page -->

{include file="frontend/components/footer.tpl"}
