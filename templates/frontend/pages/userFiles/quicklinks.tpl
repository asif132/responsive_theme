<section id="set-5">
				<div class="hi-icon-wrap hi-icon-effect-5 hi-icon-effect-5a">
					<div style="display: inline-block;font-size: 14px;"><a href="https://www.ej-med.org/index.php/ejmed/about#FocusAndScope" class="hi-icon hi-icon-archive"></a><br>
                        Focus and <br>Scope</div>
					<div style="display: inline-block;font-size: 14px;"><a href="https://www.ej-med.org/index.php/ejmed/about#AuthorFees" class="hi-icon hi-icon-contract"></a><br>
                        Article Processing <br>Fee</div>
					<div style="display: inline-block;font-size: 14px;"><a href="https://www.ej-med.org/index.php/ejmed/about#PeerReviewProcess" class="hi-icon hi-icon-pencil"></a><br>
                        Peer Review <br>Process</div>
					<div style="display: inline-block;font-size: 14px;"><a href="https://www.ej-med.org/index.php/ejmed/about/editorialTeam" class="hi-icon hi-icon-user"></a><br>
                        Editorial <br>Board</div>
					<div style="display: inline-block;font-size: 14px;"><a href="https://www.ej-med.org/index.php/ejmed/about#OpenAccessPolicy" class="hi-icon hi-icon-locked"></a><br>
                        Open Access <br>Policy</div>
					<div style="display: inline-block;font-size: 14px;"><a href="https://www.ej-med.org/index.php/ejmed/about/submissions" class="hi-icon hi-icon-support"></a><br>
                        Submissions <br>Policy</div>
					<div style="display: inline-block;font-size: 14px;"><a href="https://www.ej-med.org/index.php/ejmed/about/submissions#AuthorGuidelines" class="hi-icon hi-icon-bookmark"></a><br>
                        Author <br>Guidelines</div>
					
				</div>
				
			</section>