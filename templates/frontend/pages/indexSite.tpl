{**

 * templates/frontend/pages/indexSite.tpl

 *

 * Copyright (c) 2014-2016 Simon Fraser University Library

 * Copyright (c) 2003-2016 John Willinsky

 * Distributed under the GNU GPL v2. For full terms see the file docs/COPYING.

 *

 * Site index.

 *

 *}

{include file="frontend/components/headersite.tpl"}





<div id="main-site" class="page_index_site">
<h2 class="current_issue_title lead text-center" style="padding:6px; margin: 0 0 1em 0;letter-spacing: 1px;text-transform: uppercase;    width: 100%; margin: 20px 0">  Open <span class="Jmiddle" style="color:#ee3733">Journal </span>systems publishing   </h2>
	
		
	<div class="journals">

		



		{if !count($journals)}

			<div class="no_journals">

				{translate key="site.noJournals"}

			</div>



		{else}

			<ul class="media-list">

				{iterate from=journals item=journal}

					{capture assign="url"}{url journal=$journal->getPath()}{/capture}

					{assign var="thumb" value=$journal->getLocalizedSetting('journalThumbnail')}

					{assign var="description" value=$journal->getLocalizedDescription()}

					<li class="media  col-md-6">

						<div>

						{if $thumb}

							{assign var="altText" value=$journal->getLocalizedSetting('journalThumbnailAltText')}

							<div class="media-left">

								<a href="{$url|escape}">

									<img class="media-object" src="{$journalFilesPath}{$journal->getId()}/{$thumb.uploadName|escape:"url"}"{if $altText} alt="{$altText|escape}"{/if}>

								</a>

							</div>

						{/if}



						<div class="media-body">

							<h3 class="media-heading">

								<a href="{$url|escape}" rel="bookmark">

									{$journal->getLocalizedName()}

								</a>

							</h3>

							{if $description}

								<div class="description">

								

										{assign var = description value = $this->get_template_vars('description')}

										{assign var = lenth value =strlen($description)}

										{assign var = dtts value = ($lenth>280)?' ...':""}

										{$description.$dtts|substr:0:280|nl2br}



	

									

								</div>

							{/if}

							<ul class="nav nav-pills">

								<li class="view">

									<a href="{$url|escape}">

										{translate key="site.journalView"}

									</a>

								</li>

								<li class="current">

									<a href="{url|escape journal=$journal->getPath() page="issue" op="current"}">

										{translate key="site.journalCurrent"} 

									</a>

								</li>

								<li class="current">

									<a href="{url journal=$journal->getPath() page="about" op="submissions"}" >{translate key="about.submissions"}</a>

								</li>

							</ul>

						</div>

						</div>

					</li>

				{/iterate}

			</ul>



			{if $journals->getPageCount() > 0}

				<div class="cmp_pagination">

					{page_info iterator=$journals}

					{page_links anchor="journals" name="journals" iterator=$journals}

				</div>

			{/if}

		{/if}

	</div>



</div><!-- .page -->

{literal}

<style>

.pkp_structure_main{border-right: 0px solid #dddddd;}

.pkp_structure_main.col-md-9{width:100%;}







</style>



<script>



</script>

{/literal}



{include file="frontend/components/footer.tpl"}

