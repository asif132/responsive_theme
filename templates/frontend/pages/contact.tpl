{**
 * templates/frontend/pages/contact.tpl
 *
 * Copyright (c) 2014-2016 Simon Fraser University Library
 * Copyright (c) 2003-2016 John Willinsky
 * Distributed under the GNU GPL v2. For full terms see the file docs/COPYING.
 *
 * @brief Display the page to view the press's contact details.
 *
 * @uses $currentContext Journal|Press The current journal or press
 * @uses $mailingAddress string Mailing address for the journal/press
 * @uses $contactName string Primary contact name
 * @uses $contactTitle string Primary contact title
 * @uses $contactAffiliation string Primary contact affiliation
 * @uses $contactPhone string Primary contact phone number
 * @uses $contactEmail string Primary contact email address
 * @uses $supportName string Support contact name
 * @uses $supportPhone string Support contact phone number
 * @uses $supportEmail string Support contact email address
 *}
{include file="frontend/components/header.tpl" pageTitle="about.contact"}



<div class="page page_contact">
	{include file="frontend/components/breadcrumbs.tpl" currentTitleKey="about.contact"}
	

	{* Contact section *}

			
			<div class="row">
				
				<div class="col-md-7">
					
					<!-- Classic Heading -->
					<div class="page-header">
						<h1>Contact Us</h1>
					</div>
					
					<!-- Start Contact Form -->

					<div class="row">
					<div class="col-md-12">
					 <form role="form" class="contact-form" id="contact-form" method="post">
					    <div class="form-group">
					    <div class="controls">
					    <input type="text" placeholder="Name"  class="form-control" name="cname">
					    </div>
					    </div>
					    <div class="form-group">
					    <div class="controls">
					    <input type="email" class="email form-control" placeholder="Email" id="cemail"  name="cemail">
						<i class="fa fa-star" aria-hidden="true"></i>
					    </div>
					    </div>
					    <div class="form-group">
					    <div class="controls">
					    <input type="text" class="requiredField form-control"   placeholder="Subject" name="csubject">
					    </div>
					    </div>

					    <div class="form-group">

					    <div class="controls">
					    <textarea rows="7" placeholder="Message" name="cmessage"  class="form-control"></textarea>
					    </div>
						<div class="g-recaptcha" data-sitekey="6Le5OFIUAAAAALooF-s8nRKSQ72nYlHzGMBQCWbD"></div>
					    </div>
						<input type="hidden" name="postmail" value="go" >
					    <a href="javascript:%20check_empty()"  id="submit"  class="btn btn-default btn-large">Send</a>
					    <div id="success" style="color:#34495e;">{$mailSuccess}</div>
					</form>
					<!-- End Contact Form -->
					</div>
					</div>
				</div>
				<script>
					{literal}
						function check_empty() {
							var email=document.getElementById('cemail').value;
							var validate_email=isEmail(email);
							if(validate_email==false){
								alert('Please enter a valid email');
								//return false;
							}
							else{
								$("form#contact-form").submit();
							}
						}
						function isEmail(email) {
							var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
							return regex.test(email);
						  }
					{/literal}
				</script>
				<style>
					{literal}
						.controls .fa.fa-star {
							position: absolute;
							float: right;
							top: 59px;
							color: grey;
							right: 21px;
						}
					{/literal}
				</style>
				<div class="col-md-5">
					
						<div class="contact_section">
						
						{* Primary contact *}
						{if $contactTitle || $contactName || $contactAffiliation || $contactPhone || $contactEmail}
							<div class="contact primary">
								
								<div class="page-header">
									<h1>{translate key="about.contact.principalContact"}</h1>
								</div>
								<ul>
									{if $contactName}
									<li class="name"><i class="fa fa-user-o"></i> <strong>Name: </strong>{$contactName|escape}</li>
									{/if}
									{if $mailingAddress}
									<li class="address"><i class="fa fa-map-marker"></i>
</i> <strong>Address: </strong>{$mailingAddress|nl2br|strip_unsafe_html}</li>
									{/if}
									
									{if $contactTitle}
									<li class="title"><i class="fa fa-address-card-o"></i>
<strong> Title: </strong>{$contactTitle|escape}</li>
									{/if}
									{if $contactAffiliation}
									<li class="affiliation"><i class="fa fa-envelope-o"></i> <strong>Affiliation: </strong>{$contactAffiliation|strip_unsafe_html} </li>
									{/if}
									{if $contactPhone}
									<li class="phone"><i class="fa fa-phone"></i> <strong>{translate key="about.contact.phone"} </strong>{$contactPhone|escape}</li>
									{/if}
									{if $contactEmail}
									<li  class="email"><i class="fa fa-envelope-o"></i> <strong>Email: </strong><a href="mailto:{$contactEmail|escape}">
										{$contactEmail|escape}
									</a></li>
									{/if}
								</ul>
							</div>
						{/if}

						{* Technical contact *}
						{if $supportName || $supportPhone || $supportEmail}
							<div class="contact support">
								<div class="page-header">
									<h1>{translate key="about.contact.supportContact"}</h1>
								</div>
								
								<ul>
									{if $supportName}
									<li  class="name"> <i class="fa fa-user-o"></i> <strong>Name: </strong>{$supportName|escape}</li>	
									{/if}
									{if $supportPhone}
									<li  class="phone"><i class="fa fa-phone"></i> <strong>{translate key="about.contact.phone"} </strong>{$supportPhone|escape}</li>	
									{/if}
									{if $supportEmail}
									<li  class="email"><i class="fa fa-envelope-o"></i> <strong>Email: </strong> <a href="mailto:{$supportEmail|escape}">
										{$supportEmail|escape}
									</a></li>
									{/if}	
								</ul>
							</div>
						{/if}
					</div>

				</div>
					
					
				</div>
				
			</div>
			
	
<!-- .page -->
{literal}
<style type="text/css">
#content {
    padding: 60px 0;
}
.container {
    padding-right: 15px;
    padding-left: 15px;
    margin-right: auto;
    margin-left: auto;
}
.row {
    margin-right: -15px;
    margin-left: -15px;
}
.classic-title {
    margin-bottom: 16px;
    padding-bottom: 8px;
    border-bottom: 1px solid #eee;
    font-weight: 300;
}
.form-group {
    margin-bottom: 15px;
}
.btn-system {
    background-color: #ee3733;
}
</style>
{/literal}
{include file="frontend/components/footer.tpl"}
