{**
 * templates/frontend/pages/about.tpl
 *
 * Copyright (c) 2014-2016 Simon Fraser University Library
 * Copyright (c) 2003-2016 John Willinsky
 * Distributed under the GNU GPL v2. For full terms see the file docs/COPYING.
 *
 * @brief Display the page to view information about a journal.
 *
 * @uses $currentContext Journal|Press The current journal or press
 *}
 {if !$homeTabsJournal}
{include file="frontend/components/header.tpl" pageTitle="about.aboutContext"}
{/if}

<div id="main-content" class="page page_about">
 {if !$homeTabsJournal}
	{include file="frontend/components/breadcrumbs.tpl" currentTitleKey="about.aboutContext"}
	
	{* Page Title *}
	<div class="page-header">
		<h1>{translate key="about.aboutContext"}</h1>
	</div>
	{* /Page Title *}
	{/if}
	{$currentContext->getLocalizedSetting('about')}
</div><!-- .page -->
 {if !$homeTabsJournal}
{include file="common/frontend/footer.tpl"}
	{/if}
