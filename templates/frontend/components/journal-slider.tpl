{assign var journalPath  value = $currentJournal->_data['path']}




{if $homepageImage}
		<div class="journalInfoCol" style="background-image:url({$publicFilesDir}/{$homepageImage.uploadName|escape:"url"}) ; background-position:center center; background-size:cover">
		{else}
		<div class="journalInfoCol" style="background-image:url({$baseUrl}/plugins/themes/responsive/img/homepageImage_es_ES.jpg); background-position:center center; background-size:cover">
		{/if}






	<div id="journalSlider" class="carousel slide" data-ride="carousel">

		  <!-- Indicators -->

		  <ol class="carousel-indicators">

		    <li data-target="#journalSlider" data-slide-to="0" class="active"></li>

		    <li data-target="#journalSlider" data-slide-to="1"></li>

		    <li data-target="#journalSlider" data-slide-to="2"></li>
            
            <li data-target="#journalSlider" data-slide-to="3"></li>

		  </ol>



		  <!-- Wrapper for slides -->

		  <div class="carousel-inner" role="listbox">

		  	<!-- it is slides   -->

		    <div class="item active">

		    	<h5>Zero Tolerance for Plagiarism  </h5>

		    	<img  src="/plugins/themes/responsive/img/no_plagiarism.jpg">

		    	{include file="frontend/components/userFiles/journalName.tpl"}

				has a policy of "Zero Tolerance on the Plagiarism". We check the plagiarism issue through two methods: reviewer check and plagiarism prevention tool (iThenticate.com). All submissions will be checked by plagiarism prevention software before being sent to reviewers.
		    	
		    </div>

		    <!-- it is slides   -->

		    	<!-- it is slides   -->

		    <div class="item ">

		    	<h5>Open Access Policy </h5>

		    	<img src="/plugins/themes/responsive/img/open_access.jpg">

		    	{include file="frontend/components/userFiles/journalName.tpl"}

		    	provides immediate open access to its content on the principle that making research freely available after publication on the journal website to the public supports a greater global exchange of knowledge.
		    </div>

		    <div class="item ">

		    	<h5>Digital Archiving </h5>

		    	<img src="/plugins/themes/responsive/img/clockss_lockss.png">

			    {include file="frontend/components/userFiles/journalName.tpl"}

		    	uses LOCKSS system as digital archiving policy. LOCKSS ensures long-term survival of Web-based scholarly publications. Namely, your publication will remain digitally available forever for free under Creative Commons License.
		    </div>

		    <div class="item ">

		    	<h5>Indexing </h5>
		    	
		    	<img src="/plugins/themes/responsive/img/indexing_policy.png">   <br>

		  		{include file="frontend/components/userFiles/journalName.tpl"}


		    	is indexed with CrossRef and assigned a Digital Object Identifier (DOI). This means that all of our references are made available so that citations can be tracked by the publishing community. In addition, the journal is indexed in Google Scholar, ROAD, SCILIT, WorldCat, ScienceOpen.
		  

		    </div>

		    <!-- it is slides   -->





		  </div>



		  <!-- Controls -->

		  <a class="left carousel-control" href="#journalSlider" role="button" data-slide="prev">

		    <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>

		    <span class="sr-only">Previous</span>

		  </a>

		  <a class="right carousel-control" href="#journalSlider" role="button" data-slide="next">

		    <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>

		    <span class="sr-only">Next</span>

		  </a>

	</div>



</div>



