{**
* templates/frontend/components/primaryNavMenu.tpl
*
* Copyright (c) 2014-2016 Simon Fraser University Library
* Copyright (c) 2003-2016 John Willinsky
* Distributed under the GNU GPL v2. For full terms see the file docs/COPYING.
*
* Primary navigation menu list for OJS
*}


{assign var = journalPath  value = $currentJournal->_data['path']}



  {if $currentJournal}

  
<ul id="main-navigation" class="nav navbar-nav">
       <li id="homeMenu"><a href="{url page="index"}" class="{if $requestedPage == 'index' }active{/if}"><i class="fa fa-home"></i> {translate key="navigation.home"}</a></li> 
         <li class="dropdown">
          <a href="javascript:void(0)" class="{if ($requestedPage == 'about'  ) && ( $requestedOp != 'submissions' &&  $requestedOp != 'contact') || $requestedPage == 'information' }active{/if}" ><i class="fa fa-sitemap " aria-hidden="true"></i> {translate key="navigation.about"} <span class="caret"></span></a>
          <!--   <a href="{url router=$smarty.const.ROUTE_PAGE page="about"}" ><i class="fa fa-sitemap " aria-hidden="true"></i>  </a> -->
            <ul class="dropdown-menu">
              <li>
              <a href="{url router=$smarty.const.ROUTE_PAGE page="about"}">
                {translate key="about.aboutContext"}
              </a>
            </li>
            <li>
              <a href="{url router=$smarty.const.ROUTE_PAGE page="about" op="editorialTeam"}">
                {translate key="about.editorialTeam"}
              </a>
            </li>
            <li>
              <a href="{url router=$smarty.const.ROUTE_PAGE page="about" op="privacy"}">Privacy Statement
              </a>
            </li>
           
            <li >
              <a href="{url router=$smarty.const.ROUTE_PAGE page="about" op="contact"}">
                {translate key="navigation.contact"}
              </a>
            </li>
           
              
             
            </ul>
          </li>
        
              <li>
                <a href="{url router=$smarty.const.ROUTE_PAGE page="issue" op="current"}" class=" {if $requestedPage == 'issue' && ( $requestedOp == 'view')}active{/if}" >
                  <i class="fa fa-file-text-o" aria-hidden="true"></i> {translate key="navigation.current"}
                </a>
              </li>
              <li>
                <a href="{url router=$smarty.const.ROUTE_PAGE page="issue" op="archive"}">
                  <i class="fa fa-archive" aria-hidden="true"></i> {translate key="navigation.archives"}
                </a>
              </li>
          
          
          
          <li>
              <a href="{url router=$smarty.const.ROUTE_PAGE page="announcement"}">Announcements</a>
              </a>
            </li>
            

  </ul>
  <div class="additionalLinks">
    <a href="{url router=$smarty.const.ROUTE_PAGE page="user" op="register"}" title="" class="sf-depth-1">Subscribe</a>
    <a href="{url router=$smarty.const.ROUTE_PAGE page="about" op="submissions"}">Submission</a>
  </div>

 

 {/if}