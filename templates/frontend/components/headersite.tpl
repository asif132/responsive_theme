{**

 * lib/pkp/templates/frontend/components/header.tpl

 *

 * Copyright (c) 2014-2016 Simon Fraser University Library

 * Copyright (c) 2003-2016 John Willinsky

 * Distributed under the GNU GPL v2. For full terms see the file docs/COPYING.

 *

 * @brief Common frontend site header.

 *

 * @uses $isFullWidth bool Should this page be displayed without sidebars? This

 *       represents a page-level override, and doesn't indicate whether or not

 *       sidebars have been configured for thesite.

 *}



{* Determine whether a logo or title string is being displayed *}



{assign var = journalPath  value= $currentJournal->_data['path']}



{assign var="showingLogo" value=true}

{if $displayPageHeaderTitle && !$displayPageHeaderLogo && is_string($displayPageHeaderTitle)}

	{assign var="showingLogo" value=false}

{/if}

<!DOCTYPE html>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

<html lang="{$currentLocale|replace:"_":"-"}" xml:lang="{$currentLocale|replace:"_":"-"}">

{if !$pageTitleTranslated}{translate|assign:"pageTitleTranslated" key=$pageTitle}{/if}

<head>

	<meta http-equiv="Content-Type" content="text/html; charset={$defaultCharset|escape}">

	<meta name="viewport" content="width=device-width, initial-scale=1.0">

	<title>

		{$pageTitleTranslated|strip_tags}

		{* Add the journal name to the end of page titles *}

		{if $requestedPage|escape|default:"index" != 'index' && $currentContext && $currentContext->getLocalizedName()}

			| {$currentContext->getLocalizedName()}

		{/if}

	</title>



	{load_header context="frontend" headers=$headers}

	{load_stylesheet context="frontend" stylesheets=$stylesheets}



	

</head>



<body {if !$currentJournal} data-page="noJournal"{/if} class="pkp_page_{$requestedPage|escape|default:"index"} pkp_op_{$requestedOp|escape|default:"index"}{if $showingLogo} has_site_logo{/if}">

	<div class="pkp_structure_page">



		{* Header *}

		 {if $currentJournal}

		<input type="checkbox" name="menuOnTop" id="menuOnTop">  

          <label for="menuOnTop">

            <i class="fa fa-bars" aria-hidden="true"></i>

            <i class="fa fa-times" aria-hidden="true"></i>

          </label>

           {/if}

       

		<header class="navbar navbar-default" id="headerNavigationContainer" role="banner">

			<div class="innerHeaderWrp">

			{* User profile, login, etc, navigation menu*}

			<div class="topHeader">

				<div class="container">

					<div class="row">



						

						{*include file="frontend/components/sociallinks.tpl"*}

								

							

						

							

						

						<div id="topMenuRight" class="pull-right">

							<ul class="menu">	


	<li>

									<a href="{$baseUrl}/"><i class="fa fa-university"></i> Publisher Home</a>

								</li>
								

							



								{if $supportedLocales|@count}



								<li class="newDropDown languages" aria-haspopup="true" aria-expanded="false">

									<a href="javascript:void(0)" class=""><span class="fa fa-globe"></span> {$supportedLocales.$currentLocale}</a>

								

									<ul id="navigationUser" role="navigation" aria-label="{translate|escape key="common.navigation.user"}">

										

										{foreach from=$supportedLocales item=localeName key=localeKey}

											{if $localeKey != $currentLocale}

												<li>

													<a href="{url router=$smarty.const.ROUTE_PAGE page="user" op="setLocale" path=$localeKey source=$smarty.server.REQUEST_URI}">

														{$localeName}

													</a>

												</li>

											{/if}

										{/foreach}

									</ul>

								</li>  

								  

									{/if}

								

							{if !$hideRegisterLink}

									{if !$isUserLoggedIn}	

								<li>

									<a href="{url router=$smarty.const.ROUTE_PAGE page="user" op="register"}"><i class="fa fa-key"></i> {translate key="navigation.register"}</a>

								</li>

														

									<li><a href="{url router=$smarty.const.ROUTE_PAGE page="login"}"><i class="fa fa-user"></i> {translate key="navigation.login"}</a></li>

								{/if}

							{/if}

								 

								

								 

								{if $isUserLoggedIn}

								<li class="newDropDown">

									<a href="javascript:void(0)" class=""><i class="fa fa-user" aria-hidden="true"></i> {translate key="navigation.MyAccount"}</a>

								

									<ul id="navigationUser" role="navigation" aria-label="{translate|escape key="common.navigation.user"}">

											{if $isUserLoggedIn}

												

														{if array_intersect(array(ROLE_ID_MANAGER, ROLE_ID_ASSISTANT, ROLE_ID_REVIEWER, ROLE_ID_AUTHOR), $userRoles)}

															<li>

																<a href="{url router=$smarty.const.ROUTE_PAGE page="submissions"}">

																	{translate key="navigation.dashboard"}

																	<span class="badge">

																		{$unreadNotificationCount}

																	</span>

																</a>

															</li>

														{/if}

														<li>

															<a href="{url router=$smarty.const.ROUTE_PAGE page="user" op="profile"}">

																{translate key="common.viewProfile"}

															</a>

														</li>

														{if array_intersect(array(ROLE_ID_SITE_ADMIN), $userRoles)}

														<li>

															<a href="{if $multipleContexts}{url router=$smarty.const.ROUTE_PAGE context="index" page="admin" op="index"}{else}{url router=$smarty.const.ROUTE_PAGE page="admin" op="index"}{/if}">

																{translate key="navigation.admin"}

															</a>

														</li>

														{/if}

														<li>

															<a href="{url router=$smarty.const.ROUTE_PAGE page="login" op="signOut"}">

																{translate key="user.logOut"}

															</a>

														</li>

														{if $isUserLoggedInAs}

															<li>

																<a href="{url router=$smarty.const.ROUTE_PAGE page="login" op="signOutAsUser"}">

																	{translate key="user.logOutAs"} {$loggedInUsername|escape}

																</a>

															</li>

														{/if}

												

											

												{else}

												<li><a href="{url router=$smarty.const.ROUTE_PAGE page="login"}"><i class="fa fa-user"></i> My Profile</a></li>

												<li><a href="{url router=$smarty.const.ROUTE_PAGE page="login"}"><i class="fa fa-user"></i> {translate key="navigation.login"}</a></li>



											{/if}

											

										</ul>

								</li>

								{/if}

								<!-- <li class="searchBarWrp">

								 <form role="search" method="post" action="{url page="search" op="search"}">

			                 

					                  <input class="form-control" name="query" value="{$searchQuery|escape}" type="text" placeholder="{translate key="common.search" }" aria-label="{translate|escape key="common.searchQuery"}" placeholder="">

					                   <button type="submit" class="btn btn-default "> <span class="fa fa-search " aria-hidden="true"></span> {*translate key="common.search" *}</button>

					              </form>

					              </li> -->

							</ul>

							<div class="clearfix"></div>

						</div>				

					</div><!-- .row -->

				</div><!-- .container-fluid -->

			</div>

			
			 {if $journalPath==''}
            <div class="mainNavigation" style="padding:20px 0px;">                 
            {else}	
             <div class="mainNavigation" >  
            {/if}
            	<div class="container">

            		<div class="navbar-header">                        

					{if $displayPageHeaderLogo && is_array($displayPageHeaderLogo)}

					<a href="{$baseUrl}" >                                     

						<img src="{$publicFilesDir}/{$displayPageHeaderLogo.uploadName|escape:"url"}" alt="{$displayPageHeaderTitle.altText|escape}" class="img-responsive">

					</a>

					{else}

						<a href="{$baseUrl}"> 

							<img src="{$baseUrl}/templates/images/structure/logo.png"   class="img-responsive"/>

						</a>

					{/if}

					</div>

					

				</div>

			</div>

			<div id="primaryMenuWrp">
			{load_menu name="primary" id="main-navigation" ulClass="nav navbar-nav"}
			
			</div>
			{*include file="frontend/components/primaryNavMenu.tpl"*}

		</div> <!-- close innerHeaderWrp -->





			<!-- .pkp_head_wrapper -->

		</header><!-- .pkp_structure_head -->

		

		

		

		{if $homepageImage}

			{include file="frontend/components/journal-slider.tpl"}

			<div id="newsslide" style="display:none" >
				<div class="carousel-inner" role="listbox">
					<div id="latestnews-53" class="item active">
						<div id="latestnews-title">Recent News</div>
						<div id="latestnews-content">
							<a href=""><strong>[Publishing Articles]</strong> </a>
						</div>
					</div>
				</div> <!-- End crousel-inner -->
			</div>

		{else}

		{include file="frontend/components/inner-page-image.tpl"}

		{/if}



		{* Wrapper for page content and sidebars *}
		
		<div class="pkp_structure_content container" id="mainContainer">

			{call_hook|assign:"leftSidebarCode" name="Templates::Common::Sidebar"}

			

			{if $leftSidebarCode}

			<main class="pkp_structure_main col-md-9" role="main">

			

			{else}

			<main class="pkp_structure_main col-md-12" role="main">

			{/if}
			