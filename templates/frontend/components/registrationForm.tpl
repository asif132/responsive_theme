{**
 * templates/frontend/components/registrationForm.tpl
 *
 * Copyright (c) 2014-2016 Simon Fraser University Library
 * Copyright (c) 2003-2016 John Willinsky
 * Distributed under the GNU GPL v2. For full terms see the file docs/COPYING.
 *
 * @brief Display the basic registration form fields
 *
 * @uses $locale string Locale key to use in the affiliate field
 * @uses $firstName string First name input entry if available
 * @uses $middleName string Middle name input entry if available
 * @uses $lastName string Last name input entry if available
 * @uses $countries array List of country options
 * @uses $country string The selected country if available
 * @uses $email string Email input entry if available
 * @uses $username string Username input entry if available
 *}
<fieldset class="identity">
	<div class="page-header">
		<h1>{translate key="user.profile"}</h1>
	</div>
	
	<div class="fields">
    
    
    <div class="form-group first_name">
				<label for="givenName" class="col-sm-3 control-label">
					{translate key="user.givenName"}*
				</label>
                <div class="col-sm-9">
				<input type="text"  class="form-control" name="givenName" id="givenName" value="{$givenName|escape}" maxlength="40" placeholder="{translate key="user.givenName"}" required>
                </div>
			</div>
			
			<div class="form-group first_name">
				<label for="familyName" class="col-sm-3 control-label">
					{translate key="user.familyName"}*
				</label>
                <div class="col-sm-9">
				<input type="text" class="form-control" name="familyName" id="familyName" value="{$familyName|escape}" maxlength="40" placeholder="{translate key="user.familyName"}" required>
                </div>
			</div>
    
    
		<div class="form-group first_name">
			<label class="col-sm-3 control-label" for="firstName">
				{translate key="user.firstName"}				
			</label>
			<div class="col-sm-9">
				<input class="form-control" type="text" name="firstName" id="firstName" value="{$firstName|escape}" maxlength="40" required>
			</div>
		</div>
		<div class="form-group middle_name">
			<label class="col-sm-3 control-label" for="middleName">
				{translate key="user.middleName"}				
			</label>
			<div class="col-sm-9"><input class="form-control" type="text" name="middleName"  id="middleName" value="{$middleName|escape}" maxlength="40"></div>
		</div>
		<div class="form-group last_name">
			<label class="col-sm-3 control-label" for="lastName">
				{translate key="user.lastName"}
				
			</label>
			<div class="col-sm-9"><input class="form-control" type="text" name="lastName" id="lastName" value="{$lastName|escape}" maxlength="40" required></div>
		</div>
		<div class="form-group affiliation">
			<label class="col-sm-3 control-label" for="affiliation">
				{translate key="user.affiliation"}				
			</label>
			<div class="col-sm-9"><input class="form-control" type="text" name="affiliation[{$primaryLocale|escape}]" id="affiliation" value="{$affiliation.$primaryLocale|escape}" required></div>
		</div>
		<div class="form-group country">
			<label class="col-sm-3 control-label" for="country">
				{translate key="common.country"}				
			</label>
			<div class="col-sm-9">
				<select class="form-control" name="country" id="country" required>
					<option></option>
					{html_options options=$countries selected=$country}
				</select>
			</div>
		</div>
	</div>
</fieldset>

<fieldset class="login">
	<div class="page-header">
		<h1>{translate key="user.login"}</h1>
	</div>
	
	<div class="fields">
		<div class="form-group email">
			<label class="col-sm-3 control-label" for="email">
				{translate key="user.email"}				
			</label>
			<div class="col-sm-9"><input class="form-control" type="text" name="email" id="email" value="{$email|escape}" maxlength="90" required></div>
		</div>
		<div class="form-group username">
			<label class="col-sm-3 control-label" for="username">
				{translate key="user.username"}
				
			</label>
			<div class="col-sm-9"><input class="form-control" type="text" name="username" id="username" value="{$username|escape}" maxlength="32" required></div>
		</div>
		<div class="form-group password">
			<label class="col-sm-3 control-label" for="password">
				{translate key="user.password"}				
			</label>
			<div class="col-sm-9"><input class="form-control" type="password" name="password" id="password" password="true" maxlength="32" required></div>
		</div>
		<div class="form-group password">
			<label class="col-sm-3 control-label" for="password2">
				{translate key="user.repeatPassword"}				
			</label>
			<div class="col-sm-9">
				<input class="form-control" type="password" name="password2" id="password2" password="true" maxlength="32" required>
			</div>
		</div>
	</div>
</fieldset>
