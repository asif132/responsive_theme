{**
* templates/frontend/components/primaryNavMenu.tpl
*
* Copyright (c) 2014-2016 Simon Fraser University Library
* Copyright (c) 2003-2016 John Willinsky
* Distributed under the GNU GPL v2. For full terms see the file docs/COPYING.
*
* Primary navigation menu list for OJS
* $currentJournal = $this->get_template_vars('currentJournal'); 

*}
{assign var = journalPath  value = $currentJournal->_data['path']}

<!-- <div class="topSocial">
  <ul>
    <li><a href="https://www.facebook.com/OpenJournalSystems" class="fa fa-facebook" target="_blank"></a></li>
    <li><a href="https://twitter.com/@openjournalsys" class="fa fa-twitter"  target="_blank"></a></li>
    <li><a href="https://plus.google.com/+Openjournalsystems" class="fa fa-google-plus"  target="_blank"></a></li>
    <li><a href="https://www.linkedin.com/company/openjournalsystems-com" class="fa fa-linkedin"  target="_blank"></a></li>
  </ul>
</div> -->


	
	 {if $journalPath=='dja'}
      <div class="socialBread">			
			<span >Share</span>
			<a href="#"><i class="fa fa-envelope" aria-hidden="true"></i></a>
			<a href="#"><i class="fa fa-facebook" aria-hidden="true"></i></a>
			<a href="#"><i class="fa fa-twitter" aria-hidden="true"></i></a>
			<a href="#"><i class="fa fa-linkedin" aria-hidden="true"></i></a>
		</div>
  {elseif $journalPath=='djb'}
    <div class="socialBread">			
			<span >Share</span>
			<a href="#"><i class="fa fa-envelope" aria-hidden="true"></i></a>
			<a href="#"><i class="fa fa-facebook" aria-hidden="true"></i></a>
			<a href="#"><i class="fa fa-twitter" aria-hidden="true"></i></a>
			<a href="#"><i class="fa fa-linkedin" aria-hidden="true"></i></a>
		</div>
   {else}
   <div class="socialBread">			
			<span >Share</span>
			<a href="#"><i class="fa fa-envelope" aria-hidden="true"></i></a>
			<a href="#"><i class="fa fa-facebook" aria-hidden="true"></i></a>
			<a href="#"><i class="fa fa-twitter" aria-hidden="true"></i></a>
			<a href="#"><i class="fa fa-linkedin" aria-hidden="true"></i></a>
		</div>
  {/if}

