{**

 * templates/frontend/components/footer.tpl

 *

 * Copyright (c) 2014-2016 Simon Fraser University Library

 * Copyright (c) 2003-2016 John Willinsky

 * Distributed under the GNU GPL v2. For full terms see the file docs/COPYING.

 *

 * @brief Common site frontend footer.

 *

 * @uses $isFullWidth bool Should this page be displayed without sidebars? This

 *       represents a page-level override, and doesn't indicate whether or not

 *       sidebars have been configured for thesite.

  * {call_hook|assign:"leftSidebarCode" name="Templates::Common::Sidebar"}

 *}




	</main>





 {assign var = journalPath value = $currentJournal->_data['path']}

	{* Sidebars *}

	{if empty($isFullWidth)}

        
        {capture assign="leftSidebarCode"}{call_hook name="Templates::Common::Sidebar"}{/capture}

		{if $leftSidebarCode}

			<aside id="sidebar" class="pkp_structure_sidebar col-md-3" role="complementary" aria-label="{translate|escape key="common.navigation.sidebar"}">

				{$leftSidebarCode}

			</aside><!-- pkp_sidebar.left -->

		{/if}

	{/if}

  </div><!-- pkp_structure_content -->




<!-- footer for journal -->

 



   

	
      <div class="footerSupports">
      <a href="javascript:void(0);"><img src="{$baseUrl}/plugins/themes/responsive/img/Open_Access.jpg.png"></a>
      <a href="javascript:void(0);"><img src="{$baseUrl}/plugins/themes/responsive/img/ROAD.png"></a>
      <a href="javascript:void(0);"><img src="{$baseUrl}/plugins/themes/responsive/img/SCIIIT.png"></a>
      <a href="javascript:void(0);"><img src="{$baseUrl}/plugins/themes/responsive/img/crossref-logo-landscape-200.png"></a>
      <a href="javascript:void(0);"><img src="{$baseUrl}/plugins/themes/responsive/img/orcid_logo.png"></a>
      <a href="javascript:void(0);"><img src="{$baseUrl}/plugins/themes/responsive/img/Cope_logo_3.png"></a>
      <a href="javascript:void(0);"><img src="{$baseUrl}/plugins/themes/responsive/img/cc.logo.large.png"></a>
        
      </div>


  <footer id="footer" class="footer" role="contentinfo">

  	<div class="container">

    
    		
        
          <div class="row footer-widgets">
    
    <!-- Start Contact & Follow Widget -->
    {if $pageFooter==''}
      <div class="col-md-5 col-xs-12">
          <div class="footer-widget contact-widget">
              <h4>
                {if $displayPageHeaderLogo && is_array($displayPageHeaderLogo)}
                  <a href="{$homeUrl}" class="img-responsive" alt="Footer Logo"ss="" >
                    <img src="{$publicFilesDir}/{$displayPageHeaderLogo.uploadName|escape:"url"}" alt="{$displayPageHeaderTitle.altText|escape}">
                  </a>
                  {/if}
              </h4>
{include file="frontend/components/userFiles/footerJrnInfo.tpl"}
          </div>

       

      </div><!-- .col-md-6 -->
      <!-- End Contact Widget -->

      
      
      <div class="footer-widget col-md-4 col-xs-12 social-widget">
              <h4>Important Links<span class="head-line"></span></h4>
             
 {include file="frontend/components/userFiles/footerLinks.tpl"} 
  
      </div>
    <div class="footer-widget col-md-3 col-xs-12 social-widget">
              <h4>Contact<span class="head-line"></span></h4>
              <p>

              </p>

{include file="frontend/components/userFiles/footerContactInfo.tpl"}

              <ul class="social-icons">
                  <li>
                      <a class="facebook" href="https://www.facebook.com/" target="_blank"><span class="fa fa-facebook"></span></a>
                  </li>
                  <li>
                      <a class="twitter" href="https://twitter.com/" target="_blank"><span class="fa fa-twitter"></span></a>
                  </li>
                  <li>
                      <a class="google" href="https://plus.google.com/" target="_blank"><span class="fa fa-google-plus"></span></a>
                  </li>
                  
                  
              
                  
              </ul>
      </div>
    
    
    {/if}

    {if $pageFooter}
      {$pageFooter}
    {/if}
    
   
    <!-- End Twitter Widget -->


    <!-- Start Subscribe Widget -->
     
<!-- .col-md-3 -->
    <!-- End Subscribe Widget -->



    
</div><!-- .row -->


          
       
   
  	</div><!-- .container -->
  </footer>



<!-- footer for admin -->



  

 

</div>

{load_script context="frontend" scripts=$scripts}



{call_hook name="Templates::Common::Footer::PageFooter"}

{literal}

<style type="text/css">

#customblock-newssection { display: none}

</style>

  <script type="text/javascript">

    $(function (){





     // makeSlider('.journalInfoCol');





      var Ncount = 0;



      var news = $('#customblock-newssection p > a').length;  

     

      if(news>0)

      {  

          var __OBJECTS = [];



          $('#newsslide').show();



          $('#customblock-newssection p > a').each(function() {

              __OBJECTS.push($(this));

          });

          

          addPositioningClasses();

          

          

          var timer = 4000;

          var finaltimer = news*timer;

         

          function addPositioningClasses() {

              var card = __OBJECTS.pop();

              innHTml = $(card).wrap('<div>').parent().html();

              $('#latestnews-content').html(innHTml)

              $(card).unwrap();

              if (__OBJECTS.length) {

                  setTimeout(addPositioningClasses, timer)

              }

          }

          

          setInterval(function(){ 

              $('#customblock-newssection p > a').each(function() {

                  __OBJECTS.push($(this));

              }); 

            

            addPositioningClasses() 

          }, finaltimer);





      }// recent new section







     

  $('.searchBarWrp input').focusin(function (){

    $(this).closest('.searchBarWrp').addClass('activeState');

  }).focusout(function (){

    $(this).closest('.searchBarWrp').removeClass('activeState');

  });

  

  $('#sidebar > div .title').click(function (){

    $(this).closest('.pkp_block').toggleClass('showHide');

  });



      var baseUrl  = window.location ;

     /* var  copyBread =  $('#mainContainer .cmp_breadcrumbs').detach();

       $('.breadcrumsWrpHD').prepend(copyBread);*/



      //alert(baseUrl)

     $('#main-navigation a[href="'+baseUrl+'"]').addClass('active');



        $('body').on('click', '.show-search',function(e){

            var flag = $('#seachCheckFlag').prop('checked');

          

            if(flag==true){

                $('#seachCheckFlag').prop('checked', false);

               // if( $('#searchWrpNav form input').val()!=''){

                    setTimeout(function (){

                        $('#searchWrpNav button[type="submit"]').click();

                    }, 1000);

               // }

            }else{

                 $('#seachCheckFlag').prop('checked', true);

            }

           

        });



          //slider
        function makeSlider (id){
             var images = '';
             var Icount = 0;
             $(id+' img').each(function (i){
              Icount++;
                var activeClass="";
                if(i==0){
                    activeClass="active";
                }

                 images += '<div class="item '+activeClass+'">\
                              <img src="'+ $(this).attr('src')+'"/>\
                            </div>';
            });
             if(Icount==0) return;
             
               var slidertemplate = '<div id="carousel-example-generic" class="carousel slide" data-ride="carousel">\
                              <!-- Indicators -->\
                              <ol class="carousel-indicators">\
                                <li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>\
                                <li data-target="#carousel-example-generic" data-slide-to="1"></li>\
                                <li data-target="#carousel-example-generic" data-slide-to="2"></li>\
                              </ol>\
                              <!-- Wrapper for slides -->\
                              <div class="carousel-inner" role="listbox">\
                                '+images+'\
                              </div>\
                              <!-- Controls -->\
                              <a class="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev">\
                                <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>\
                                <span class="sr-only">Previous</span>\
                              </a>\
                              <a class="right carousel-control" href="#carousel-example-generic" role="button" data-slide="next">\
                                <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>\
                                <span class="sr-only">Next</span>\
                              </a>\
                            </div>';
                    $(id).html(slidertemplate);
            $('.carousel').carousel();

        }

        makeSlider('#customblock-Slider .content .content');

       

      





    });


    $(document).ready(function () {
        $('.dropdown-toggle').dropdown();
    });

	
	</script>

  {/literal}

</body>

</html>

