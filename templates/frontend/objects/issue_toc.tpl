{**
 * templates/frontend/objects/issue_toc.tpl
 *
 * Copyright (c) 2014-2016 Simon Fraser University Library
 * Copyright (c) 2003-2016 John Willinsky
 * Distributed under the GNU GPL v2. For full terms see the file docs/COPYING.
 *
 * @brief View of an Issue which displays a full table of contents.
 *
 * @uses $issue Issue The issue
 * @uses $issueTitle string Title of the issue. May be empty
 * @uses $issueSeries string Vol/No/Year string for the issue
 * @uses $issueGalleys array Galleys for the entire issue
 * @uses $hasAccess bool Can this user access galleys for this context?
 * @uses $showGalleyLinks bool Show galley links to users without access?
 * @ to show image in some versons
 *	{if $issue->getLocalizedCoverImageUrl()}
 *  	<img class="img-responsive" src="{$issue->getLocalizedCoverImageUrl()|escape}"{if $issue->getCoverImageAltText('en_US') != ''} alt="{$issue->getCoverImageAltText('en_US')|escape}"{/if}>
  *  {else}
  *    	<img class="img-responsive" src="{$coverImagePath|escape}{$issueCover|escape}"{if $issue->getCoverImageAltText('en_US') != ''} alt="{$issue->getCoverImageAltText('en_US')|escape}"{/if}>
  *  {/if}
 *}
 
 {* In getCoverImageAltText we must pass 'en_US' *}
<div class="issue-toc">

	{* Indicate if this is only a preview *}
	{if !$issue->getPublished()}
		{include file="frontend/components/notification.tpl" type="warning" messageKey="editor.issues.preview"}
	{/if}

	{* Issue introduction area above articles *}
	<div class="heading">
		
		{* Issue cover image and description*}
		
		{assign var=issueCover value=$issue->getCoverImage('en_US')}
		{if $issueCover}
			<div class="thumbnail">
				<a class="cover" href="{url op="view" page="issue" path=$issue->getBestIssueId()}">
					
     				<img class="img-responsive" src="{$publicFilesDir}/{$issueCover|escape}"{if $issue->getCoverImageAltText('en_US') != ''} alt="{$issue->getCoverImageAltText('en_US')|escape}"{/if}>
    
				</a>
				
			</div>
		{if $issue->hasDescription()}
			<div class="description">
				{$issue->getLocalizedDescription()|strip_unsafe_html}
			</div>
		{/if}

		{elseif $issue->hasDescription()}
			<div class="description">
				{$issue->getLocalizedDescription()|strip_unsafe_html}
			</div>
		{/if}

		{* PUb IDs (eg - DOI) *}
		{foreach from=$pubIdPlugins item=pubIdPlugin}
			{if $issue->getPublished()}
				{assign var=pubId value=$issue->getStoredPubId($pubIdPlugin->getPubIdType())}
			{else}
				{assign var=pubId value=$pubIdPlugin->getPubId($issue)}{* Preview pubId *}
			{/if}
			{if $pubId}
				{assign var="doiUrl" value=$pubIdPlugin->getResolvingURL($currentJournal->getId(), $pubId)|escape}
				<div class="pub_id {$pubIdPlugin->getPubIdType()|escape}">
					<span class="type">
						{$pubIdPlugin->getPubIdDisplayType()|escape}:
					</span>
					<span class="id">
						{if $doiUrl}
							<a href="{$doiUrl|escape}">
								{$doiUrl}
							</a>
						{else}
							{$pubId}
						{/if}
					</span>
				</div>
			{/if}
		{/foreach}

		{* Published date *}
		{* if $issue->getDatePublished()}
			<div class="published">
				<strong>
					{translate key="submissions.published"}:
				</strong>
				{$issue->getDatePublished()|date_format:$dateFormatShort}
			</div>
		{/if *}
	</div>

	{* Full-issue galleys *}
	{if $issueGalleys && ($hasAccess || $showGalleyLinks)}
		<div class="galleys"> sds
			<div class="page-header">
				<h2>
					<small>{translate key="issue.fullIssue"}</small>
				</h2>
			</div>
			<div class="btn-group" role="group">
				{foreach from=$issueGalleys item=galley}
					{include file="frontend/objects/galley_link.tpl" parent=$issue}
				{/foreach}
			</div>
		</div>
	{/if}
    
    
  {* Articles *}  
    <div class="sections issueTocPublishArticles">

  <!-- Nav tabs -->
  <ul class="nav nav-tabs" role="tablist">

    
<!--     <li role="presentation"><a href="#Case_Study" aria-controls="Case_Study" role="tab" data-toggle="tab">Case Study</a></li>
    <li role="presentation"><a href="#Review" aria-controls="Review" role="tab" data-toggle="tab">Review</a></li> -->
    
    
    {assign var=row value=1}
    
    {foreach name=sections from=$publishedSubmissions item=section}
				{if $section.articles}
					{if $section.title}
                    <li role="presentation" class="{if $row eq 1}active{/if}"><a href="#{$section.title|replace:' ':'_'}" aria-controls="{$section.title|replace:' ':'_'}" role="tab" data-toggle="tab">{$section.title|escape}</a></li>
					{/if}
				{/if}
         {assign var=row value=$row+1}
	{/foreach}
    
    
  </ul>

  <!-- Tab panes -->
  <div class="tab-content">

    
    {assign var=row value=1}
    
    {foreach name=sections from=$publishedSubmissions item=section}
    
	<div role="tabpanel" class="tab-pane {if $row eq 1}active{/if}" id="{$section.title|replace:' ':'_'}">		
            <section class="section">
				{if $section.articles}
					{if $section.title}
						<h2 class="current_issue_title lead">
							{$section.title|escape}
						</h2>
						<!-- <div class="page-header">
							<h2>
								{$section.title|escape}
							</h2>
						</div> -->
					{/if}
					<div class="media-list">
						{foreach from=$section.articles item=article}
							{include file="frontend/objects/article_summary.tpl"}
						{/foreach}
					</div>
				{/if}
			</section>
         </div>
         {assign var=row value=$row+1}
	{/foreach}
    
    
    
  </div>

</div><!-- .sections -->
    
	
	
</div><!-- .issue-toc -->
