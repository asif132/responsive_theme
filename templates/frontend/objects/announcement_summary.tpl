{**
 * templates/frontend/objects/announcement_summary.tpl
 *
 * Copyright (c) 2014-2016 Simon Fraser University Library
 * Copyright (c) 2003-2016 John Willinsky
 * Distributed under the GNU GPL v2. For full terms see the file docs/COPYING.
 *
 * @brief Display a summary view of an announcement
 *
 * @uses $announcement Announcement The announcement to display
 *}
<article class="announcement-summary media">
	<div class="media-body">
		<h2 class="media-heading">
			<span class="fa fa-pencil themeEditIcon" ></span>
			<div class="titleWithThemeEditIcon">
				<a href="{url router=$smarty.const.ROUTE_PAGE page="announcement" op="view" path=$announcement->getId()}">
					{$announcement->getLocalizedTitle()|escape}
				</a>
				<small class="date">
					<span class="glyphicon glyphicon-calendar"></span>
					{$announcement->getDatePosted()}
				</small>
			</div>
		</h2>
		<div class="announcementContent">
		{$announcement->getLocalizedDescriptionShort()|strip_unsafe_html} <br />
		<a class="galley-link btn btn-default role="  href="{url router=$smarty.const.ROUTE_PAGE page="announcement" op="view" path=$announcement->getId()}">Read more <i class="fa fa-angle-right"></i></a>
		</div>
	</div>
<br><br>
</article><!-- .announcement-summary -->
