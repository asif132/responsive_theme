{**
 * templates/frontend/objects/article_summary.tpl
 *
 * Copyright (c) 2014-2016 Simon Fraser University Library
 * Copyright (c) 2003-2016 John Willinsky
 * Distributed under the GNU GPL v2. For full terms see the file docs/COPYING.
 *
 * @brief View of an Article summary which is shown within a list of articles.
 *
 * @uses $article Article The article
 * @uses $hasAccess bool Can this user access galleys for this context? The
 *       context may be an issue or an article
 * @uses $showGalleyLinks bool Show galley links to users without access?
 *}
{assign var=articlePath value=$article->getBestArticleId($currentJournal)}
{if (!$section.hideAuthor && $article->getHideAuthor() == $smarty.const.AUTHOR_TOC_DEFAULT) || $article->getHideAuthor() == $smarty.const.AUTHOR_TOC_SHOW}
	{assign var="showAuthor" value=true}
{/if}

<div class="article-summary media">
	{if $article->getCoverImage('en_US')}
		
	{/if}

	<div class="media-body">
		<div class=" WrpMediaCnt">
		<h3 class="media-heading">
			<a href="{url page="article" op="view" path=$articlePath}">
				{$article->getLocalizedTitle()|strip_unsafe_html}
			</a>
		</h3>

		{if $showAuthor || $article->getPages() || ($article->getDatePublished() && $showDatePublished)}

			{if $showAuthor}
				<div class="meta">
					{if $showAuthor}
						<div class="authors">
							{$article->getAuthorString()}
						</div>
					{/if}
				</div>
			{/if}

		</div>	

			<div class="galleryLinksWrp">
				{foreach from=$article->getGalleys() item=galley}
					{include file="frontend/objects/galley_link.tpl" parent=$article}
				{/foreach}
				{* Page numbers for this article *}
			{if $article->getPages()}
				<p class="pages" align="center">
					{$article->getPages()|escape}
				</p>
			{/if}
			</div>
		{/if}

	</div>

	{call_hook name="Templates::Issue::Issue::Article"}
 
{* this section is the DOI section*}
 <br>
{assign var=pubId value=$article->getStoredPubId('doi')}
{if $pubId}
<strong><img src="/plugins/themes/responsive/img/icon-doi.png"> DOI </strong>
<a href="https://doi.org/{$pubId|escape:'url'}">
    https://doi.org/{$pubId}
</a>
{/if}

</br>
<img src="/plugins/themes/responsive/img/icon-graph.png">{translate key="article.abstract"} views: {$article->getViews()} , {if is_a($article, 'PublishedArticle')}{assign var=galleys value=$article->getGalleys()}{/if}

{if !$hideGalleys}



{foreach from=$article->getGalleys() item=galley}

<img src="/plugins/themes/responsive/img/icon-pdf.png">{$galley->getGalleyLabel()} downloads: {$galley->getViews()}

{* Indexing Links *}
<div class="index_grid">
	<div class="index_column1">
	</div>	
	<div class="index_column">
		<a href="https://doi.org/{$pubId|escape:'url'}" target="_blank" >
			<img src="/plugins/themes/responsive/img/doi_icon.png"  title="DOI Link" style="  width: 40px; height: 40px"> 
		</a>
	</div>
	<div class="index_column">
		<a href="https://scholar.google.com/scholar?hl=en&as_sdt=0%2C5&q='{$article->getLocalizedTitle()|escape}'" target="_blank" >
			<img src="/plugins/themes/responsive/img/google_scholar_icon.svg"  title="View in Google Scholar" style="  width: 40px; height: 40px"> 
		</a>
	</div>
	<div class="index_column">
		<a href="https://www.worldcat.org/search?q={$article->getLocalizedTitle()|escape}" target="_blank">
			<img src="/plugins/themes/responsive/img/worldcat_icon.png"  title="View in WorldCat" style="width: 40px; height: 40px"> 
		</a>
	</div>
	<div class="index_column">
		<a href="https://www.scilit.net/articles/search?q={$article->getLocalizedTitle()|escape}" target="_blank">
			<img src="/plugins/themes/responsive/img/scilit_icon.png"  title="View in Scilit" style="width: 40px; height: 40px"> 
		</a>
	</div>
	<div class="index_column">
		<a href="https://academic.microsoft.com/search?q={$article->getLocalizedTitle()|escape}" target="_blank">
			<img src="/plugins/themes/responsive/img/microsoft_academia_icon.png"  title="View in Microsoft Academia" style="width: 40px; height: 40px"> 
		</a>
	</div>


</div>

{/foreach}

{/if}
</div><!-- .article-summary -->
