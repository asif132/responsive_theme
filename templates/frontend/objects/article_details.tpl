{**
 * templates/frontend/objects/article_details.tpl
 *
 * Copyright (c) 2014-2021 Simon Fraser University
 * Copyright (c) 2003-2021 John Willinsky
 * Distributed under the GNU GPL v3. For full terms see the file docs/COPYING.
 *
 * @brief View of an Article which displays all details about the article.
 *  Expected to be primary object on the page.
 *
 * Many journals will want to add custom data to this object, either through
 * plugins which attach to hooks on the page or by editing the template
 * themselves. In order to facilitate this, a flexible layout markup pattern has
 * been implemented. If followed, plugins and other content can provide markup
 * in a way that will render consistently with other items on the page. This
 * pattern is used in the .main_entry column and the .entry_details column. It
 * consists of the following:
 *
 * <!-- Wrapper class which provides proper spacing between components -->
 * <div class="item">
 *     <!-- Title/value combination -->
 *     <div class="label">Abstract</div>
 *     <div class="value">Value</div>
 * </div>
 *
 * All styling should be applied by class name, so that titles may use heading
 * elements (eg, <h3>) or any element required.
 *
 * <!-- Example: component with multiple title/value combinations -->
 * <div class="item">
 *     <div class="sub_item">
 *         <div class="label">DOI</div>
 *         <div class="value">12345678</div>
 *     </div>
 *     <div class="sub_item">
 *         <div class="label">Published Date</div>
 *         <div class="value">2015-01-01</div>
 *     </div>
 * </div>
 *
 * <!-- Example: component with no title -->
 * <div class="item">
 *     <div class="value">Whatever you'd like</div>
 * </div>
 *
 * Core components are produced manually below, but can also be added via
 * plugins using the hooks provided:
 *
 * Templates::Article::Main
 * Templates::Article::Details
 *
 * @uses $article Submission This article
 * @uses $publication Publication The publication being displayed
 * @uses $firstPublication Publication The first published version of this article
 * @uses $currentPublication Publication The most recently published version of this article
 * @uses $issue Issue The issue this article is assigned to
 * @uses $section Section The journal section this article is assigned to
 * @uses $categories Category The category this article is assigned to
 * @uses $primaryGalleys array List of article galleys that are not supplementary or dependent
 * @uses $supplementaryGalleys array List of article galleys that are supplementary
 * @uses $keywords array List of keywords assigned to this article
 * @uses $pubIdPlugins Array of pubId plugins which this article may be assigned
 * @uses $licenseTerms string License terms.
 * @uses $licenseUrl string URL to license. Only assigned if license should be
 *   included with published submissions.
 * @uses $ccLicenseBadge string An image and text with details about the license
 *}
 
 
<article class="article-details">
	
<header>
		<h2 class="page-header">
			{$article->getLocalizedTitle()|escape}
			{if $article->getLocalizedSubtitle()}
				<small>
					{$article->getLocalizedSubtitle()|escape}
				</small>
			{/if}
		</h2>
	</header>
	<div class="row">

		<section class="article-sidebar col-md-4">

			{* Screen-reader heading for easier navigation jumps *}
			<h2 class="sr-only">{translate key="plugins.themes.bootstrap3.article.sidebar"}</h2>



			{* Article/Issue cover image *}
			{if $article->getCoverImage('en_US') || $issue->getCoverImage('en_US')}
				<div class="cover-image">
					{if $article->getCoverImage('en_US')}
						<img class="img-responsive" src="{$publicFilesDir}/{$article->getLocalizedCoverImage()|escape}"{if $article->getLocalizedCoverImageAltText('en_US')} alt="{$article->getLocalizedCoverImageAltText('en_US')|escape}"{/if}>
					{else}
						<a href="{url page="issue" op="view" path=$issue->getBestIssueId()}">
							<img class="img-responsive" src="{$publicFilesDir}/{$issue->getLocalizedCoverImage()|escape}"{if $issue->getLocalizedCoverImageAltText('en_US')} alt="{$issue->getLocalizedCoverImageAltText('en_US')|escape}"{/if}>
						</a>
					{/if}
				</div>
			{/if}

			{* Article Galleys *}
			{if $article->getGalleys()}
				<div class="download">
					{foreach from=$article->getGalleys() item=galley}
						{include file="frontend/objects/galley_link.tpl" parent=$article}
					{/foreach}
				</div>
			{/if}
<div class="list-group">

				{* Published date *}
				{if $article->getDatePublished()}
					<div class="list-group-item date-published">
						<strong>Submitted</strong> {$article->getDateSubmitted()|date_format} <br>
						<strong>{translate key="submissions.published"}</strong>
						{$article->getDatePublished()|date_format}
					</div>
				{/if}

				{* DOI (requires plugin) *}
				{foreach from=$pubIdPlugins item=pubIdPlugin}
					{if $pubIdPlugin->getPubIdType() != 'doi'}
						{continue}
					{/if}
					{if $issue->getPublished()}
						{assign var=pubId value=$article->getStoredPubId($pubIdPlugin->getPubIdType())}
					{else}
						{assign var=pubId value=$pubIdPlugin->getPubId($article)}{* Preview pubId *}
					{/if}
					{if $pubId}
						{assign var="doiUrl" value=$pubIdPlugin->getResolvingURL($currentJournal->getId(), $pubId)|escape}
						<div class="list-group-item doi">
							<strong><img src="/plugins/themes/responsive/img/icon-doi.png">{translate key="plugins.pubIds.doi.readerDisplayName"}</strong>
							<a href="{$doiUrl}">
								{$doiUrl}
							</a>
						</div>
					{/if}
				{/foreach}
				
			</div>
			<div class="panel panel-default section">	<img src="/plugins/themes/responsive/img/icon-graph.png"> <strong>{translate key="article.abstract"} viewed =  {$article->getViews()} times</strong>

                {if is_a($article, 'PublishedArticle')}{assign var=galleys value=$article->getGalleys()}{/if}</br>

                {if $galleys}

                {foreach from=$galleys item=galley name=galleyList}

                <img src="/plugins/themes/responsive/img/icon-pdf.png"> <strong>{$galley->getGalleyLabel()} downloaded =  {$galley->getViews()} times</strong>

                {/foreach}
    
                {/if}</div>
			{* Issue article appears in *}
				<div class="panel panel-default issue">
					<div class="panel-heading">
						{translate key="issue.issue"}
					</div>
					<div class="panel-body">
						<a class="title" href="{url page="issue" op="view" path=$issue->getBestIssueId($currentJournal)}">
							{$issue->getIssueIdentification()}
						</a>

					</div>
				</div>

				{if $section}
					<div class="panel panel-default section">
						<div class="panel-heading">
							{translate key="section.section"}
						</div>
						<div class="panel-body">
							{$section->getLocalizedTitle()|escape}
						</div>
					</div>
				{/if}
				
				

		</section><!-- .article-sidebar -->

		<div class="col-md-8">
			<section class="article-main">

				{* Screen-reader heading for easier navigation jumps *}
				<h2 class="sr-only">{translate key="plugins.themes.bootstrap3.article.main"}</h2>

				{if $article->getAuthors()}
				<div class="author_row">
				<div class="author_column">
                    <ul class="csv">
                        {foreach from=$article->getAuthors() item=author}
                                <li class="author">
										
											<i class="fa fa-user" style='font-size: 85%;color: #00619c;'></i>&thinsp; {$author->getFullName()|escape}{if $author->getOrcid()} <a span class="orcid" href="{$author->getOrcid()|escape}" target="_blank"><img src="/plugins/themes/responsive/img/orcidimage.png" width: 75%; height: 75%;></a>{/if}</li></br>
										
										{* author's biography *}
											
							
							{if $author->getLocalizedAffiliation()}
								<div class="article-author-affilitation">
						<i class="fa fa-university" style='font-size: 85%;color: #00619c;'></i>	&thinsp;&thinsp;&thinsp;<em>{$author->getLocalizedAffiliation()|escape}</em>
								</div>
							{/if}
							{if $author->getOrcid()}
								<span class="orcid">
									<a href="{$author->getOrcid()|escape}" target="_blank">
										
									</a>
								</span>
							{/if}
						
	
								</li>
						{/foreach}
					</ul>
				</div>
{/if}
 
		 {* Dimentions Badge *}
		<div class="author_column1">
			<span class="__dimensions_badge_embed__" data-doi="{$pubId}" data-legend="hover-right"></span>
			<script async src="https://badge.dimensions.ai/badge.js" charset="utf-8"></script>
		</div>	
		</div>

		{* Indexing Links *}
		<div class="index_grid">
			<div class="index_column1">
			</div>	
			<div class="index_column">
				<a href="{$doiUrl}" target="_blank" >
					<img src="/plugins/themes/responsive/img/doi_icon.png"  title="DOI Link" style="  width: 48px; height: 48px"> 
				</a>
			</div>
			<div class="index_column">
				<a href="https://scholar.google.com/scholar?hl=en&as_sdt=0%2C5&q='{$article->getLocalizedTitle()|escape}'" target="_blank" >
					<img src="/plugins/themes/responsive/img/google_scholar_icon.svg"  title="View in Google Scholar" style="  width: 48px; height: 48px"> 
				</a>
			</div>
			<div class="index_column">
				<a href="https://www.worldcat.org/search?q={$article->getLocalizedTitle()|escape}" target="_blank">
					<img src="/plugins/themes/responsive/img/worldcat_icon.png"  title="View in WorldCat" style="width: 48px; height: 48px"> 
				</a>
			</div>
			<div class="index_column">
				<a href="https://www.scilit.net/articles/search?q={$article->getLocalizedTitle()|escape}" target="_blank">
					<img src="/plugins/themes/responsive/img/scilit_icon.png"  title="View in Scilit" style="width: 48px; height: 48px"> 
				</a>
			</div>
			<div class="index_column">
				<a href="https://academic.microsoft.com/search?q={$article->getLocalizedTitle()|escape}" target="_blank">
					<img src="/plugins/themes/responsive/img/microsoft_academia_icon.png"  title="View in Microsoft Academia" style="width: 48px; height: 48px"> 
				</a>
			</div>


		</div>

				{* Article abstract *}
				{if $article->getLocalizedAbstract()}
					<div class="article-summary" id="summary">
						<div class="article-text"><h2>{translate key="article.abstract"}</h2></div>
						<div class="article-abstract">
							{$article->getLocalizedAbstract()|strip_unsafe_html|nl2br}
						</div>
					</div>
				{/if}
<br/>
			
			{if !empty($keywords[$currentLocale])}
			<div class="item-keywords">
				<a style="color:black"><strong>Keywords: </strong></a> {foreach from=$keywords item=keyword}
						{foreach name=keywords from=$keyword item=keywordItem}
							{$keywordItem|escape}{if !$smarty.foreach.keywords.last}, {/if}
						{/foreach}
					{/foreach}
				
			</div>
			{/if}
			<br/>
			<div class="panel panel-default section">
            {* References *}
			{if $parsedCitations || $publication->getData('citationsRaw')}
			<div class="article-text"><h2>References</h2></div>
            
				
				
					<div class="value">
						{if $parsedCitations}
							{foreach from=$parsedCitations item="parsedCitation"}
								<p>{$parsedCitation->getCitationWithLinks()|strip_unsafe_html} {call_hook name="Templates::Article::Details::Reference" citation=$parsedCitation}</p>
							{/foreach}
						{else}
							{$publication->getData('citationsRaw')|escape|nl2br}
						{/if}
					</div>
				
			</div>
			{/if}
            
				{call_hook name="Templates::Article::Main"}

			</section><!-- .article-main -->

			<section class="article-more-details">

				{* Screen-reader heading for easier navigation jumps *}
				<h2 class="sr-only">{translate key="plugins.themes.bootstrap3.article.details"}</h2>

				{* Citation formats *}
				{if $citation}
					<div class="panel panel-default citation_formats">
						<div class="panel-heading">
							{translate key="submission.howToCite"}
						</div>
						<div class="panel-body">

							{* Output the first citation format *}
							<div id="citationOutput" role="region" aria-live="polite">
                                    {$citation}
                                </div>
                                <div class="citation_formats dropdown">
                                    <button class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                        {translate key="submission.howToCite.citationFormats"}
                                    </button>
                                    <div class="dropdown-menu" aria-labelledby="dropdownMenuButton" id="dropdown-cit">
                                        {foreach from=$citationStyles item="citationStyle"}
                                            <a
                                                    class="dropdown-cite-link dropdown-item"
                                                    aria-controls="citationOutput"
                                                    href="{url page="citationstylelanguage" op="get" path=$citationStyle.id params=$citationArgs}"
                                                    data-load-citation
                                                    data-json-href="{url page="citationstylelanguage" op="get" path=$citationStyle.id params=$citationArgsJson}"
                                            >
                                                {$citationStyle.title|escape}
                                            </a>
                                        {/foreach}
                                        {if count($citationDownloads)}
                                            <div class="dropdown-divider"></div>
                                            <h4 class="download-cite">
                                                {translate key="submission.howToCite.downloadCitation"}
                                            </h4>
                                            {foreach from=$citationDownloads item="citationDownload"}
                                                <a class="dropdown-item" href="{url page="citationstylelanguage" op="download" path=$citationDownload.id params=$citationArgs}">
                                                    <span class="fa fa-download"></span>
                                                    {$citationDownload.title|escape}
                                                </a>
                                            {/foreach}
                                        {/if}
                                    </div>
                                </div>
						</div>
					</div>
				{/if}

				{* PubIds (requires plugins) *}
				{foreach from=$pubIdPlugins item=pubIdPlugin}
					{if $pubIdPlugin->getPubIdType() == 'doi'}
						{continue}
					{/if}
					{if $issue->getPublished()}
						{assign var=pubId value=$article->getStoredPubId($pubIdPlugin->getPubIdType())}
					{else}
						{assign var=pubId value=$pubIdPlugin->getPubId($article)}{* Preview pubId *}
					{/if}
					{if $pubId}
						<div class="panel panel-default pub_ids">
							<div class="panel-heading">
								{$pubIdPlugin->getPubIdDisplayType()|escape}
							</div>
							<div class="panel-body">
								{if $pubIdPlugin->getResolvingURL($currentJournal->getId(), $pubId)|escape}
									<a id="pub-id::{$pubIdPlugin->getPubIdType()|escape}" href="{$pubIdPlugin->getResolvingURL($currentJournal->getId(), $pubId)|escape}">
										{$pubIdPlugin->getResolvingURL($currentJournal->getId(), $pubId)|escape}
									</a>
								{else}
									{$pubId|escape}
								{/if}
							</div>
						</div>
					{/if}
				{/foreach}

				{* Article Subject *}
				{if $article->getLocalizedSubject()}
					<div class="panel panel-default subject">
						<div class="panel-heading">
							{translate key="article.subject"}
						</div>
						<div class="panel-body">
							{$article->getLocalizedSubject()|escape}
						</div>
					</div>
				{/if}

				

				{* Licensing info *}
				{if $copyright || $licenseUrl}
					<div class="panel panel-default copyright">
						<div class="panel-body">
							{if $licenseUrl}
								{if $ccLicenseBadge}
									{$ccLicenseBadge}
								{else}
									<a href="{$licenseUrl|escape}" class="copyright">
										{if $copyrightHolder}
											{translate key="submission.copyrightStatement" copyrightHolder=$copyrightHolder copyrightYear=$copyrightYear}
										{else}
											{translate key="submission.license"}
										{/if}
									</a>
								{/if}
							{/if}
							{$copyright}
						</div>
					</div>
				{/if}
				
			

				{call_hook name="Templates::Article::Details"}

			</section><!-- .article-details -->
		</div><!-- .col-md-8 -->
	</div><!-- .row -->

</article>

